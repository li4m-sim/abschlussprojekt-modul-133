-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 06. Jul 2021 um 12:11
-- Server-Version: 10.4.19-MariaDB
-- PHP-Version: 7.4.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `employeedb`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `employee`
--

CREATE TABLE `employee` (
                            `Id` int(11) NOT NULL,
                            `Firstname` text NOT NULL,
                            `Lastname` text NOT NULL,
                            `Email` text NOT NULL,
                            `Gender` text NOT NULL,
                            `Birthday` date NOT NULL,
                            `Role` text NOT NULL,
                            `Salary` int(11) NOT NULL,
                            `StartAtWork` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `employee`
--

INSERT INTO `employee` (`Id`, `Firstname`, `Lastname`, `Email`, `Gender`, `Birthday`, `Role`, `Salary`, `StartAtWork`) VALUES
(1, 'Liam', 'Simcic', 'liam.troll@emailch', 'MALE', '2003-09-13', 'FRONTEND_DEVELOPER', 1000, '2021-06-07'),
(2, 'Alexander', 'Meier', 'alex@mail.com', 'MALE', '2011-07-06', 'CEO', 1000, '2021-07-05'),
(3, 'Timon', 'Weidmann', 'timon@mail.com', 'MALE', '2004-07-06', 'APPRENTICE', 500, '2020-07-05'),
(4, 'Katrin', 'Widmer', 'kadwidm@mail.com', 'FEMALE', '2002-05-26', 'APPRENTICE', 500, '2020-07-05'),
(5, 'Hans', 'Reiner', 'blueball@mail.com', 'MALE', '1992-05-26', 'BUSINESS_ANALYST', 5000, '2005-07-05'),
(6, 'Reyna', 'Liva', 'reynaliva@mail.com', 'FEMALE', '1999-05-26', 'PRATICAL_TRAINER', 6000, '2011-07-05'),
(7, 'Carmen', 'Riva', 'carmenriva@mail.com', 'FEMALE', '1980-05-26', 'SCRUMMASTER', 2500, '2014-09-15'),
(10, 'Parker', 'Enrich', 'penrich9@earthlink.net', 'Male', '1974-05-06', 'APPRENTICE', 4988, '1987-04-11'),
(11, 'Brocky', 'Hallagan', 'bhallagana@tumblr.com', 'Male', '1988-03-31', 'FRONTEND_DEVELOPER', 4951, '1988-11-02'),
(12, 'Annabal', 'Waddams', 'awaddamsb@aol.com', 'Female', '1986-07-16', 'APPRENTICE', 6575, '1990-07-07'),
(13, 'Jeane', 'Coxall', 'jcoxallc@typepad.com', 'Female', '1963-02-14', 'BUSINESS_ANALYST', 6121, '1988-12-27'),
(14, 'Lilias', 'Davydochkin', 'ldavydochkind@issuu.com', 'Male', '1964-01-25', 'APPRENTICE', 5995, '2004-08-19'),
(15, 'Barrie', 'Speares', 'bspearese@businessweek.com', 'Male', '1967-11-17', 'BUSINESS_ANALYST', 4213, '2009-05-14'),
(16, 'Shaun', 'Cadany', 'scadanyf@cloudflare.com', 'Female', '1993-02-14', 'BUSINESS_ANALYST', 5576, '1980-12-31'),
(17, 'Bernete', 'Dalinder', 'bdalinderg@homestead.com', 'Male', '1977-09-10', 'FRONTEND_DEVELOPER', 2009, '2012-12-28'),
(18, 'Galina', 'Lawrance', 'glawranceh@sciencedaily.com', 'Female', '1985-05-15', 'FRONTEND_DEVELOPER', 2099, '1996-11-16'),
(19, 'Mirabella', 'Arnaudon', 'marnaudoni@storify.com', 'Male', '1984-12-13', 'APPRENTICE', 5159, '1997-10-31'),
(20, 'Gino', 'Comford', 'gcomfordj@cyberchimps.com', 'Male', '1980-07-23', 'BUSINESS_ANALYST', 2361, '1982-06-26'),
(21, 'Cara', 'Preist', 'cpreistk@delicious.com', 'Female', '1984-09-21', 'BUSINESS_ANALYST', 4014, '2008-05-04'),
(22, 'Bennie', 'Prodrick', 'bprodrickl@wikia.com', 'Male', '1992-05-02', 'BACKEND_DEVELOPER', 6054, '2008-06-19'),
(23, 'Jodee', 'Karus', 'jkarusm@businessinsider.com', 'Female', '1994-04-10', 'BUSINESS_ANALYST', 6662, '2006-10-09'),
(24, 'Jarib', 'Pinwell', 'jpinwelln@boston.com', 'Male', '1977-04-04', 'APPRENTICE', 4692, '1991-10-13'),
(25, 'Etti', 'Abbes', 'eabbeso@purevolume.com', 'Female', '1962-07-22', 'PRATICAL_TRAINER', 4447, '2014-08-17'),
(26, 'Lorne', 'Mariette', 'lmariettep@canalblog.com', 'Male', '1960-07-24', 'FRONTEND_DEVELOPER', 2313, '1994-02-27'),
(27, 'Maggi', 'Darell', 'mdarellq@myspace.com', 'Male', '1978-08-21', 'APPRENTICE', 3941, '2001-08-12'),
(28, 'Fredelia', 'Swatland', 'fswatlandr@cyberchimps.com', 'Female', '1978-06-05', 'PRATICAL_TRAINER', 5074, '2012-04-08'),
(29, 'Clo', 'Burkinshaw', 'cburkinshaws@wikipedia.org', 'Male', '1999-01-05', 'FRONTEND_DEVELOPER', 5679, '2011-03-02'),
(30, 'Fidel', 'Robathon', 'frobathont@vistaprint.com', 'Male', '1977-05-10', 'BACKEND_DEVELOPER', 2257, '2007-04-13'),
(31, 'Dun', 'Cominotti', 'dcominottiu@squidoo.com', 'Male', '1976-05-12', 'BACKEND_DEVELOPER', 3884, '1986-02-15'),
(32, 'Patrick', 'Hustings', 'phustingsv@amazon.co.uk', 'Male', '1994-06-12', 'FRONTEND_DEVELOPER', 2490, '2000-05-15'),
(33, 'Stefa', 'de Vaen', 'sdevaenw@goo.gl', 'Male', '1976-08-07', 'PRATICAL_TRAINER', 2220, '2008-08-29'),
(34, 'Winona', 'Escalero', 'wescalerox@wp.com', 'Male', '1969-05-27', 'FRONTEND_DEVELOPER', 3878, '1993-09-22'),
(35, 'Willow', 'Vain', 'wvainy@diigo.com', 'Female', '1997-04-25', 'PRATICAL_TRAINER', 3869, '2013-05-13'),
(36, 'Darrelle', 'Skewis', 'dskewisz@umn.edu', 'Female', '1969-09-04', 'PRATICAL_TRAINER', 4037, '1999-06-25'),
(37, 'Rawley', 'Eate', 'reate10@netvibes.com', 'Male', '1969-10-03', 'BACKEND_DEVELOPER', 2565, '1990-01-06'),
(38, 'Hallie', 'Kynnd', 'hkynnd11@gravatar.com', 'Male', '1991-06-15', 'BACKEND_DEVELOPER', 6324, '1987-03-23'),
(39, 'Templeton', 'Lawther', 'tlawther12@un.org', 'Female', '1961-08-26', 'APPRENTICE', 6614, '2015-05-17'),
(40, 'Madlin', 'Lymbourne', 'mlymbourne13@who.int', 'Female', '1962-05-06', 'FRONTEND_DEVELOPER', 4396, '2013-03-24');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `employee`
--
ALTER TABLE `employee`
    ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `employee`
--
ALTER TABLE `employee`
    MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

package ch.bbw.lsam.abschlussprojekt;

import ch.bbw.lsam.abschlussprojekt.db.EmployeeDao;
import ch.bbw.lsam.abschlussprojekt.model.Employee;
import ch.bbw.lsam.abschlussprojekt.model.attributes.Gender;
import ch.bbw.lsam.abschlussprojekt.model.attributes.Role;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Controller
public class EmployeeController {

    private final EmployeeDao employeeDao;

    public EmployeeController(EmployeeDao employeeDao) {
        this.employeeDao = employeeDao;
    }

    @GetMapping("/employee-table")
    public String getEmployeeTable(Model model){
        List<Employee> employeeList = new ArrayList<>();
        try{
            employeeList = employeeDao.getEmployees();
        } catch (SQLException exception){
            exception.printStackTrace();;
        }

        model.addAttribute("employees", employeeList);

        return "employeetable";
    }

    @GetMapping("/edit/{id}")
    public String editPerson(@PathVariable Integer id, Model model){
        Employee employee;
        List<Role> roles = Arrays.asList(Role.APPRENTICE, Role.CEO, Role.PRODUCTOWNER,
                Role.BACKEND_DEVELOPER, Role.SCRUMMASTER, Role.FRONTEND_DEVELOPER, Role.BUSINESS_ANALYST,
        Role.PRATICAL_TRAINER);
        try{
            employee = employeeDao.getPersonById(id);
            model.addAttribute("Employee", employee);
            model.addAttribute("roles", roles);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return "editPerson";
    }

    @PostMapping("/edit/{id}")
    public String refreshPerson(@Valid @ModelAttribute("Employee") Employee employee, BindingResult bindingResult, Model model){
        if(bindingResult.hasErrors()){
            System.out.println(bindingResult.getAllErrors().toString());
            return "redirect:/error";
        }
        System.out.println("inside methode");
        try {
            System.out.println(employee.toString());
            employeeDao.refreshPerson(employee);
        }catch (Exception e){
            e.printStackTrace();
        }

        return "redirect:/";
    }

    public static void main(String[] args) throws SQLException {
        EmployeeDao dao = new EmployeeDao();
        Employee e = new Employee(1, "Liam", "Simcic", "liam@gay.com", LocalDate.now(), LocalDate.now(), 1200, Gender.MALE, Role.FRONTEND_DEVELOPER);
        dao.refreshPerson(e);
    }

}

package ch.bbw.lsam.abschlussprojekt.db;

import java.sql.*;

import ch.bbw.lsam.abschlussprojekt.model.Employee;
import ch.bbw.lsam.abschlussprojekt.model.attributes.Gender;
import ch.bbw.lsam.abschlussprojekt.model.attributes.Role;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Repository
public class EmployeeDao {
    private static final Logger log = Logger.getLogger(EmployeeDao.class.getSimpleName());

    List<Employee> employees = new ArrayList<>();
    Connection connection;

    @PostConstruct
    private void init() {
        log.info("--------MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.jdbc.Driver");
            log.info("the driver is loaded");
        } catch (ClassNotFoundException e) {
            log.info("Where is yourMySQL JDBC Driver?");
            e.printStackTrace();
        }
        try {// Verbindung aufbauen
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeedb", "root", "");
        } catch (SQLException e) {
            log.info("Connection Failed! Check output console");
            e.printStackTrace();
        }
    }

    @PreDestroy
    public void destroy() {
        try {
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    public List<Employee> getEmployees() throws SQLException {// Hier kommt die Verarbeitung der Datenbank auslesen und anzeigen
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeedb", "root", "");
        Statement stmt = connection.createStatement();
        String sql_query = "Select id, firstname, lastname, email, birthday, salary, startAtWork, gender, role from employee";
        try {
            ResultSet rs = stmt.executeQuery(sql_query);
            while (rs.next()) {

                Gender gender = getGender(rs.getString(8));
                Role role = getRole(rs.getString(9));

                Employee employee = new Employee(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5).toLocalDate(),
                        rs.getDate(7).toLocalDate(),
                        rs.getDouble(6),
                        gender,
                        role
                );
                employees.add(employee);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return employees;
    }

    public Gender getGender(String genderString){
        Gender gender = switch (genderString) {
            case "MALE" -> Gender.MALE;
            case "FEMALE" -> Gender.FEMALE;
            default -> Gender.OTHER;
        };
        return gender;
    }

    public Role getRole(String roleString){
        Role role = switch (roleString) {
            case "PRODUCTOWNER" -> Role.PRODUCTOWNER;
            case "APPRENTICE" -> Role.APPRENTICE;
            case "BACKEND_DEVELOPER" -> Role.BACKEND_DEVELOPER;
            case "BUSINESS_ANALYST" -> Role.BUSINESS_ANALYST;
            case "FRONTEND_DEVELOPER" -> Role.FRONTEND_DEVELOPER;
            case "PRATICAL_TRAINER" -> Role.PRATICAL_TRAINER;
            case "CEO" -> Role.CEO;
            case "SCRUMMASTER" -> Role.SCRUMMASTER;
            default -> Role.BACKEND_DEVELOPER;
        };
        return role;
    }

    public Employee getPersonById(Integer id) throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeedb", "root", "");
        Statement stmt = connection.createStatement();
        String sql_query = "Select id, firstname, lastname, email, birthday, salary, startAtWork, gender, role from employee where Id='"+id+"'";

        try {
            ResultSet rs = stmt.executeQuery(sql_query);
            if (rs.next()) {

                Gender gender = getGender(rs.getString(8));
                Role role = getRole(rs.getString(9));

                Employee employee = new Employee(
                        rs.getInt(1),
                        rs.getString(2),
                        rs.getString(3),
                        rs.getString(4),
                        rs.getDate(5).toLocalDate(),
                        rs.getDate(7).toLocalDate(),
                        rs.getDouble(6),
                        gender,
                        role
                );
                return employee;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void refreshPerson(Employee employee) throws SQLException {
        connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/employeedb", "root", "");
        try {
            PreparedStatement stmt = connection.prepareStatement("UPDATE employee SET firstname = ?, lastname = ?, email = ?, salary = ?, role = ?  where Id =?");
            stmt.setString(1, employee.getFirstname());
            stmt.setString(2, employee.getLastname());
            stmt.setString(3, employee.getEmail());
            stmt.setDouble(4, employee.getSalary());
            stmt.setObject(5, employee.getRole());
            stmt.setInt(6, employee.getId());

            stmt.executeUpdate();
            stmt.close();

        }catch (SQLException se){
            se.getErrorCode();
        }



    }


}


package ch.bbw.lsam.abschlussprojekt.model.attributes;

public enum Role {
    CEO,
    FRONTEND_DEVELOPER,
    BACKEND_DEVELOPER,
    PRODUCTOWNER,
    SCRUMMASTER,
    BUSINESS_ANALYST,
    APPRENTICE,
    PRATICAL_TRAINER
}

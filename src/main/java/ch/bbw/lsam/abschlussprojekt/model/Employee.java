package ch.bbw.lsam.abschlussprojekt.model;

import ch.bbw.lsam.abschlussprojekt.model.attributes.Gender;
import ch.bbw.lsam.abschlussprojekt.model.attributes.Role;
import org.hibernate.validator.constraints.Range;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Size;
import java.time.LocalDate;

public class Employee {

    private int id;
    @NotEmpty
    @Size(min = 1, max = 40)
    private String firstname, lastname, email;
    @NotNull
    @Past(message="Datum muss in der Vergangenheit sein")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate birthday, startAtWorkPlace;
    @NotNull
    @Range(min = 1, max = 3000000)
    private double salary;
    @NotEmpty
    @Size(min = 1, max = 7)
    private Gender gender;
    @NotNull
    private Role role;

    public Employee(int id, String firstname, String lastname, String email, LocalDate birthday, LocalDate startAtWorkPlace, double salary, Gender gender, Role role) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.birthday = birthday;
        this.startAtWorkPlace = startAtWorkPlace;
        this.salary = salary;
        this.gender = gender;
        this.role = role;
    }

    public Employee(){
        id=2;
        firstname = "";
        lastname = "";
        email = "";
        birthday = LocalDate.now();
        startAtWorkPlace = LocalDate.now();
        salary = 0;
        gender = Gender.MALE;
        role = Role.APPRENTICE;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public LocalDate getStartAtWorkPlace() {
        return startAtWorkPlace;
    }

    public void setStartAtWorkPlace(LocalDate startAtWorkPlace) {
        this.startAtWorkPlace = startAtWorkPlace;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstname='" + firstname + '\'' +
                ", lastname='" + lastname + '\'' +
                ", email='" + email + '\'' +
                ", birthday=" + birthday +
                ", startAtWorkPlace=" + startAtWorkPlace +
                ", salary=" + salary +
                ", gender=" + gender +
                ", role=" + role +
                '}';
    }
}

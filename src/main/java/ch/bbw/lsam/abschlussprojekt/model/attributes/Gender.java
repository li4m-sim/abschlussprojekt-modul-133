package ch.bbw.lsam.abschlussprojekt.model.attributes;

public enum Gender {
    MALE,
    FEMALE,
    OTHER
}
